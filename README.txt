README.txt
==========
The Favicon by Paths module allows you to customize the favicon of your site on
a page-by-page basis.



INSTALLATION
============
Unzip the plugin into its own folder in your modules folder, ie:
sites/all/modules/favicon_by_path or sites/<your_site>/modules/favicon_by_path.

Dependencies
------------
  - [CTools](http://drupal.org/project/ctools)



USAGE
=====
Go to the configuration page at Configuration -> Media -> Favicon by Path or
http://<your-drupal-site>/admin/config/media/favicon_by_path. From here you can
add rules for your favicons and their corresponding paths.

Each rule comprises four fields:


Favicon Path
------------
This sets the favicon to be loaded by this rule. This can have several formats,
depending on where you placed your favicon. You can place it in any path
relative to the drupal root, such as:

  sites/all/themes/my_theme/favicon.ico

    or

  profiles/my_profile/themes/my_theme/favicon.ico

Or in the files directory at sites/all/files/ or sites/my_site/files/ using a
file stream wrapper, or a simple relative path:

  public://favicons/myfavicon.ico

    or

  favicons/myfavicon.ico

The module will look for your file relative to the drupal root before it looks
in the files directory.


Paths
-----
This determines the Drupal paths that, when matching the current path, triggers
the favicon rule. Enter one path per line. General wildcards are allowed, such
as *, ? and [] as per the PHP function fnmatch().

  node/*/edit
  node/gr[ae]y
  *

Token substitution is also available, with a number of replacements available.

  node/[node:title]
  user/[user:name]

The two may be combined to form complex rules:

  foo/*/bar/[node:title]


Enabled
-------
Indicates whether the rule is active, allowing you to temporarily disable
certain rules. Keep in mind that browsers *aggressively* cache
favicons--if you a disable a rule without replacing it, most browsers will still
show the last favicon it cached.


Weight
------
Set by dragging-and-dropping rules into the order you want. Determines the order
in which favicon rules are applied. The rules are ordered from lowest tohighest
weight, and applied in that order. Rule matching stops when the first match is
made, so ensure your more generic rules are at the bottom.



MAINTAINER
=============
Chris Kahn (aptorian)
http://coldfrontlabs.ca
