<?php
/**
 * @file
 * Favicon_by_path admin include file.
 *
 * @author Chris Kahn <ckahn@coldfrontlabs.ca>
 */

/**
 * Lists the existing favicon rules in a draggable form.
 */
function favicon_by_path_favicons_form($form, &$form_state) {
  // Prevent Drupal from 'flattening' our multi-row form variables.
  $form['#tree'] = TRUE;
  $favicons = favicon_by_path_load();
  $count = 0;

  // Add existing favicons to the form.
  foreach ($favicons as $item) {
    $is_overridden = ($item->export_type & EXPORT_IN_CODE) ? TRUE : FALSE;
    $form['favicons'][$item->fid] = array(
      'favicon' => array(
        '#type' => 'textfield',
        '#default_value' => $item->favicon,
        '#size' => 50,
      ),
      'paths' => array(
        '#type' => 'textarea',
        '#default_value' => implode("\n", $item->paths),
      ),
      'enabled' => array(
        '#type' => 'checkbox',
        '#default_value' => $item->enabled,
      ),
      'weight' => array(
        '#type' => 'textfield',
        '#default_value' => $count,
        '#size' => 3,
        '#title_display' => 'invisible',
        '#title' => t('Weight for favicon rule'),
      ),
      'operations' => array(
        '#type' => 'markup',
        '#markup' => l(t($is_overridden ? 'revert' : 'delete'), "admin/config/media/favicon_by_path/delete/" . $item->fid),
      ),
    );
    $count++;
  }

  // Add the "Add new favicon" row to the bottom.
  $form['favicons']['_add_new_favicon'] = array(
    'favicon' => array(
      '#type' => 'textfield',
      '#default_value' => '',
      '#prefix' => '<div class="label-input" style="float: left;"><div class="add-new-favicon">' . t('Add new favicon') . '</div>',
      '#suffix' => '</div>',
      '#size' => 50,
    ),
    'paths' => array(
      '#type' => 'textarea',
      '#default_value' => '',
    ),
    'enabled' => array(
      '#type' => 'checkbox',
      '#default_value' => 1,
    ),
    'weight' => array(
      '#type' => 'textfield',
      '#size' => 3,
      '#default_value' => $count++,
      '#title_display' => 'invisible',
      '#title' => t('Weight for favicon rule'),
    ),
  );

  $form['token_help'] = array(
    '#title' => t('Replacement patterns'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    'help' => array(
      '#theme' => 'token_tree',
      '#token_types' => array('node', 'user'),
    ),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save changes'),
  );

  return $form;
}

/**
 * Themes the draggable favicons form.
 */
function theme_favicon_by_path_favicons_form(&$vars) {
  $form = $vars['form'];
  $rows = array();

  foreach (element_children($form['favicons']) as $id) {
    $form['favicons'][$id]['weight']['#attributes']['class'] = array('favicon-rule-weight');
    $rows[] = array(
      'data' => array(
        drupal_render($form['favicons'][$id]['favicon']),
        drupal_render($form['favicons'][$id]['paths']),
        drupal_render($form['favicons'][$id]['enabled']),
        drupal_render($form['favicons'][$id]['weight']),
        drupal_render($form['favicons'][$id]['operations']),
      ),
      'class' => array('draggable'),
    );
  }

  $header = array(
    t('Favicon Path'),
    t('Paths'),
    t('Enabled'),
    t('Weight'),
    t('Operations'),
  );

  $table_id = 'favicon-by-path-table';

  $output = theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array('id' => $table_id),
  ));
  $output .= drupal_render_children($form);

  drupal_add_tabledrag($table_id, 'order', 'sibling', 'favicon-rule-weight');

  return $output;
}

/**
 * Handles favicon form submission for a new rule.
 */
function favicon_by_path_favicons_form_submit($form, &$form_state) {
  foreach ($form_state['values']['favicons'] as $fid => $submitted_favicon) {
    if ($fid == '_add_new_favicon') {
      // Only create a new favicon if both the favicon path and applicable
      // path rules are given.
      if (empty($submitted_favicon['favicon']) &&
          empty($submitted_favicon['paths'])
      ) {
        continue;
      }

      $favicon = ctools_export_crud_new('favicon_by_path');
      $favicon->fid = uniqid();
    }
    else {
      $favicon = ctools_export_crud_load('favicon_by_path', check_plain($fid));
    }

    $favicon->favicon = check_plain($submitted_favicon['favicon']);
    $favicon->paths   = array_map("trim", explode("\n", check_plain($submitted_favicon['paths'])));
    $favicon->enabled = intval($submitted_favicon['enabled']);
    $favicon->weight  = intval($submitted_favicon['weight']);

    favicon_by_path_save($favicon);
  }
}

/**
 * Confirm delete form for deleting a favicon path rule.
 */
function favicon_by_path_favicons_delete_confirm($form, &$form_state, $favicon) {
  $form['fid'] = array('#type' => 'value', '#value' => $favicon->fid);
  $op = ($favicon->export_type & EXPORT_IN_CODE) ? 'Revert' : 'Delete';
  $message = t("Are you sure you want to @action the favicon rule %title?",
    array('@action' => t(drupal_strtolower($op)), '%title' => $favicon->favicon));
  $caption = '<p>' . t('This action cannot be undone.') . '</p>';

  return confirm_form($form,
                     $message,
                     'admin/config/media/favicon_by_path',
                     $caption,
                     t($op));
}

/**
 * Handles deletion of favicon path rule.
 */
function favicon_by_path_favicons_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm'] &&
      isset($form_state['values']['fid']) &&
      !empty($form_state['values']['fid']) &&
      $favicon = ctools_export_crud_load('favicon_by_path', $form_state['values']['fid'])
  ) {
    ctools_export_crud_delete('favicon_by_path', $favicon->fid);

    // Deleting an export in code will revert it.
    $op = ($favicon->export_type & EXPORT_IN_CODE) ? 'Reverted' : 'Deleted';
    $message = t("Favicon rule %favicon has been @action.",
    array(
      '%favicon' => $favicon->favicon,
      '@action' => t(drupal_strtolower($op)))
    );
    drupal_set_message($message);
  }
  else {
    drupal_set_message(t("There was an error deleting favicon %id.",
      array('%id' => $form_state['values']['fid'])));
  }
  $form_state['redirect'] = 'admin/config/media/favicon_by_path';
}
